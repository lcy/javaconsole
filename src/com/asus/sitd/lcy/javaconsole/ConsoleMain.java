package com.asus.sitd.lcy.javaconsole;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

public class ConsoleMain extends ClassA {

    private static SimpleDateFormat _formater = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

    public void add() {
        this.i++;
    }

    @Override
    public int getValue() {
        return this.i;
    }

    public static String sayHello() {
        return "Hello";
    }

    public static String[] convertUTF8(String[] data) {
        data[0] = data[0].substring(1);
        return data;
    }

    static void testInputParams(String... strings) {
        System.out.println("***************Start***************");
        System.out.println(Arrays.asList(strings).toString());
        System.out.println("****************End****************");
    }

    public static void testList() {
        List<String> list = new ArrayList<String>();
        list.add("test1");
        list.add(null);
        System.out.println("***************Start***************");
        for (String string : list) {
            System.out.println(string);
        }
        System.out.println(list.contains(null));
        System.out.println("****************End****************");
    }

    public static void testJodaInterval() {
        System.out.println("***************Start***************");
        DateTime now = new DateTime();
        Interval time = new Interval(now.getMillis(), now.plusDays(1).getMillis());
        System.out.println(_formater.format(new Date(time.getStartMillis())) + "-" + _formater.format(new Date(time.getEndMillis())));
        time = time.withStartMillis(now.plusHours(2).getMillis());
        System.out.println(_formater.format(new Date(time.getStartMillis())) + "-" + _formater.format(new Date(time.getEndMillis())));
        System.out.println(now.getMillisOfDay());
        System.out.println("****************End****************");
    }

    public static void testCalendarTimezone() {
        System.out.println("***************Start***************");
        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(Long.valueOf("1386777600000"));
        System.out.println(_formater.format(d.getTime()));
        //		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        d.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println(_formater.format(d.getTime()));
        //		System.out.println(sdf.format(new Date(Long.valueOf("1377734400000"))));
        System.out.println("****************End****************");
    }

    public static void testJodaTime() {
        System.out.println("***************Start***************");
        DateTime dt = new DateTime();
        System.out.println(_formater.format(dt.getMillis()));
        System.out.println(dt.getMillisOfDay());
        System.out.println(_formater.format(dt.getMillis() - dt.getMillisOfDay()));

        DateTime era = new DateTime(0);
        System.out.println(_formater.format(era.getMillis() - era.getMillisOfDay()));

        System.out.println(">> Now: " + _formater.format(DateTime.now().toDate()));
        System.out.println(">> After minus: " + _formater.format(DateTime.now().minusDays(2).toDate()));
        System.out.println("****************End****************");
    }

    public static void testGregorianCalendar() {
        System.out.println("***************Start***************");
        Calendar gc = GregorianCalendar.getInstance();
        gc.set(2013, 8, 24, 15, 30, 0);
        System.out.println(gc.getTimeInMillis());
        System.out.println("****************End****************");
    }

    public static void testCalendar() {
        System.out.println("***************Start***************");
        Calendar c = Calendar.getInstance();
        //		c.setTimeZone(TimeZone.getTimeZone("UTC"));
        c.set(2013, 8, 24, 15, 30, 0);
        System.out.println(c.getTimeInMillis());
        System.out.println("****************End****************");
    }

    public static void testExtendsClass() {
        System.out.println("***************Start***************");
        ConsoleMain console = new ConsoleMain();
        System.out.println(console.getValue());
        console.add();
        System.out.println(console.getValue());
        System.out.println(console.i);
        console.i++;
        System.out.println(console.getValue());

        class ClassB extends ConsoleMain {

        }
        ClassB classB = new ClassB();
        System.out.println(classB.i);
        classB.i++;
        System.out.println(classB.i);
        System.out.println("****************End****************");
    }

    public static void testEnum() {
        System.out.println("***************Start***************");
        System.out.println(EnumType.abcd.toString() + "\n");
        for (EnumType type : EnumType.values()) {
            System.out.println(type);
        }
        System.out.println("****************End****************");
    }

    public static void testStringArray() {
        System.out.println("***************Start***************");
        String[] array = new String[] { "jerry", "aaron", "jaspter", "nokia", "david", "charlie", "hm" };
        List<String> list = Arrays.asList(array);
        System.out.println(list.indexOf("aaron"));

        List<String[]> arrayList = new ArrayList<String[]>();
        arrayList.add(array);
        arrayList.add(new String[] { "chocolin" });
        System.out.println(arrayList.size());

        for (String[] strings : arrayList) {
            System.out.println(strings.length);
        }

        String[] cArray = ConsoleMain.getColumnArray(arrayList.toArray(new String[2][7]), 1);
        for (String string : cArray) {
            System.out.println(string);
        }
        System.out.println(">> Array content: " + list.toString());
        System.out.println("****************End****************");
    }

    public static void testFileCodeing() {
        System.out.println("***************Start***************");
        for (String string : ConsoleMain.readFile("files2.txt", "")) {
            System.out.println(string.replace("\uFEFF", "").length());
            System.out.println(string);
            System.out.println(string.length());
            byte[] tmp = null;
            try {
                tmp = string.getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
            }
            System.out.println(new String(tmp).length());
        }
        System.out.println("****************End****************");
    }

    public static void testJodaIntervalIsSameTime() {
        System.out.println("***************Start***************");
        long now = System.currentTimeMillis();
        Interval interval = new Interval(now, now);
        System.out.println(">> Same Interval: " + interval.getStartMillis() + "/" + interval.getEndMillis());
        System.out.println("****************End****************");
    }

    public static void testReferenceParameter(List<String> strings) {
        strings.add("Add string from method.");
    }

    public static void testDateTimeString() {
        System.out.println("***************Start***************");
        System.out.println(DateTime.now().toLocalDate().toString().replace("-", ""));
        System.out.println("****************End****************");
    }

    public static void test() {
        System.out.println("***************Start***************");
        String emails = "akls@kalsjd.com;ajkgk@akjdl.com,aljgw0oe@askljdf.com";
        for (String email : emails.split("[,;]")) {
            System.out.println(email);
        }

//        Set<String> set = new TreeSet<String>();
//        set.add(EnumType.ijkl.name());
//        set.add(EnumType.abcd.name());
//        set.add(EnumType.efgh.name());
//        set.add(EnumType.abcd.name());
//        System.out.println("Set test: " + set);
        System.out.println("****************End****************");
    }

    public static void testTimeWithTimezone() {
        DateTime time = new DateTime(DateTimeZone.forID("America/Sao_Paulo"));
        System.out.println(">> Time: " + time.getMillis());
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // NOTE Test console
        ConsoleMain.testList();

        ConsoleMain.testJodaInterval();

        ConsoleMain.testCalendarTimezone();

        ConsoleMain.testJodaTime();

        ConsoleMain.testGregorianCalendar();

        ConsoleMain.testCalendar();

        ConsoleMain.testExtendsClass();

        ConsoleMain.testEnum();

        ConsoleMain.testFileCodeing();

        ConsoleMain.testStringArray();

        ConsoleMain.testJodaIntervalIsSameTime();

        ConsoleMain.test();

        ConsoleMain.testInputParams("abc", "cde");

        String[] str = new String[] {"1", "2"};
        System.out.println(Arrays.asList(str).toString());

//        String abc = "asdgas";
//        abc.contains(null);

        HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
        hashMap.put(null, 2);
        System.out.println(hashMap.toString());
        System.out.println(hashMap.getOrDefault("ab", 10));

        List<String> list = new ArrayList<String>();
        list.add("1st string");
        ConsoleMain.testReferenceParameter(list);
        System.out.println(list);

        int count = 1;
        System.out.println(">> Result=" + count++);

        System.out.println(EnumType.class.getSimpleName().concat("___"));

        ConsoleMain.testTimeWithTimezone();

        String str1 = "Test";
        String str2 = "test";
        System.out.println(">> Is same? " + str1.compareToIgnoreCase(str2));

        boolean b1 = true;
        boolean b2 = false;
        System.out.println(">> Is same? " + (b1 == b2));

        String[] array = new String[0];
        System.out.println(">> array count: " + array.length);

        List<Long> test = new ArrayList<Long>(0);
        test.add((long) 0);
        test.add((long) 1);
        System.out.println(">> array size: " + test);

        for (EnumType type : EnumType.values()) {
            System.out.println(">> type: " + type);
        }
    }

    public static String[] getColumnArray(String[][] array, int coluIndex) {
        String[] newArray = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            if (i < array[i].length) {
                newArray[i] = array[i][coluIndex];
            } else {
                newArray[i] = new String();
            }
        }
        return newArray;
    }

    public static String[] readFile(String file, String split) {
        List<String> data = new ArrayList<String>();
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new DataInputStream(fis), "UTF-8"));
            String str = null;
            while ((str = reader.readLine()) != null) {
                data.add(str);
            }
            reader.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.toArray(new String[data.size()]);
    }

}
