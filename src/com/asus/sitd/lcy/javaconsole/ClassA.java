package com.asus.sitd.lcy.javaconsole;

public class ClassA {
	
	protected int i = 0;
	
	public int getValue() {
		return this.i;
	}
	
	public void setValue(int i) {
		this.i = i;
	}

}
